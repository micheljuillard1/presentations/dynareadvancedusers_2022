(TeX-add-style-hook
 "bigmodels_mj_2022"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "handout")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("mathpazo" "sc") ("nicefrac" "nice")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "inputenc"
    "fontenc"
    "amsmath"
    "amssymb"
    "mathtools"
    "pgf"
    "tikz"
    "tikz-qtree"
    "mathpazo"
    "nicefrac"
    "epstopdf"
    "graphicx"
    "pstricks"))
 :latex)

