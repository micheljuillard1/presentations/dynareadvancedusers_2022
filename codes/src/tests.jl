using Dynare

context = @dynare "/data/projects/DynareJulia/Dynare.jl/test/models/exampe1_pf/example1_pf";

model = context.models[1]
results = context.results.model_results[1]
work = context.work
params = work.params
steadystate = trends.endogenous_steady_state
exogenous_steadystate = trends.exogenous_steady_state

periods = 610

Y0 = repeat(steadystate, n)
X = repeat(exogenous_steadystate, n)
dynamic_variables = zeros(3*model.endogenous_nbr)
temp_vec = 

f! = Dynare.make_pf_residuals(
    steadystate,
    steadystate,
    X,
    dynamic_variables,
    steadystate,
    params,
    model,
    periods,
    temp_vec,
)

J! = Dynare.make_pf_jacobian(
    Dynare.DFunctions.dynamic_derivatives!,
    initialvalues,
    terminalvalues,
    dynamic_variables,
    exogenous,
    periods,
    temp_vec,
    params,
    steadystate,
    model.dynamic_g1_sparse_colptr,
    nzval,
    model.endogenous_nbr,
    model.exogenous_nbr,
    perfect_foresight_ws.permutations,
    nzval1
)
